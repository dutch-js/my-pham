@extends('frontend.layouts.app')
@section('title', 'index')
@section('content')
<!---->
<div style="background: url({{asset('storage/slide/'.$slide->image)}}) no-repeat 0px 0px; background-size: cover;min-height: 700px;">
    <div class="container">
    </div>
</div>

<div class="welcome">
    <div class="container">
        <div class="col-md-3 welcome-left">
            <h2>Welcome to our site</h2>
        </div>
        <div class="col-md-9 welcome-right">
            <h3>Proin ornare massa eu enim pretium efficitur.</h3>
            <p>Etiam fermentum consectetur nulla, sit amet dapibus orci sollicitudin vel.
                Nulla consequat turpis in molestie fermentum. In ornare, tellus non interdum ultricies, elit
                ex lobortis ex, aliquet accumsan arcu tortor in leo. Nullam molestie elit enim. Donec ac
                aliquam quam, ac iaculis diam. Donec finibus scelerisque erat, non convallis felis commodo ac.</p>
        </div>
    </div>
</div>
<div class="featured">
    <div class="container">
        <h3>Sản phẩm</h3>
        <div class="feature-grids">
            @forelse($products as $product)
            <div class="col-md-3 feature-grid jewel">
                <a href="{{route('frontend.product.detail', $product->id)}}">
                    <img src="{{asset('storage/product/'.$product->image)}}" alt="{{$product->name}}"/>
                    <div class="arrival-info">
                        <h4>{{$product->name}}</h4>
                        <p>{{number_format($product->price,0,",",".")}} vnđ</p>
                    </div>
                    <div class="viw">
                        <a href="{{route('cart.product', $product->id)}}"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>Thêm vào giỏ hàng</a>
                    </div>
            </div>
                @empty
                <div><h2 class="text-center">Không có dữ liệu</h2></div>
            @endforelse
    </div>
</div>
<!---->
    @if(count($newProduct) > 0)
<div class="arrivals">
    <div class="container">
        <h3>Sản phẩm mới nhất</h3>
        <div class="arrival-grids">
            <ul id="flexiselDemo1">
                @foreach($newProduct as $product)
                <li>
                    <a href="{{route('frontend.product.detail', $product->id)}}"><img src="{{asset('storage/product/'.$product->image)}}" alt="{{$product->name}}"/>
                        <div class="arrival-info">
                            <h4>{{$product->name}}</h4>
                            <p>{{$product->price}} vnđ</p>
                        </div>
                        <div class="viw">
                            <a href="{{route('cart.product', $product->id)}}"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>Thêm vào giỏ hàng</a>
                        </div>
                    </a>
                </li>
                @endforeach
            </ul>
            <script src="{{ asset('web/js/jquery.min.js') }}"></script>
            <script type="text/javascript">
                $(window).load(function() {
                    $("#flexiselDemo1").flexisel({
                        visibleItems: 4,
                        animationSpeed: 1000,
                        autoPlay: true,
                        autoPlaySpeed: 3000,
                        pauseOnHover:true,
                        enableResponsiveBreakpoints: true,
                        responsiveBreakpoints: {
                            portrait: {
                                changePoint:480,
                                visibleItems: 1
                            },
                            landscape: {
                                changePoint:640,
                                visibleItems: 2
                            },
                            tablet: {
                                changePoint:768,
                                visibleItems: 3
                            }
                        }
                    });
                });
            </script>
            <script type="text/javascript" src="{{asset('web/js/jquery.flexisel.js')}}"></script>
        </div>
    </div>
</div>
</div>
    @endif

@endsection