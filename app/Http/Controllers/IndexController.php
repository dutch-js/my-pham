<?php

namespace App\Http\Controllers;

use App\Product;
use App\Language;
use App\Brand;
use App\Slide;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index(Request $request)
    {
        $newProduct = Product::select('id', 'quantity', 'image', 'name', 'desc', 'price AS price')
            ->limit(4)
            ->orderBy('id', 'DESC')
            ->get();
        $slide = Slide::select('id', 'image')->orderBy('id', 'DESC')->first();

        $product = Product::select('id', 'quantity', 'image', 'price' , 'name', 'desc');
        if ($request->has('search') && !empty($request->input('search')))  {
            $product->where('name_en', 'like', "%{$request->input('search')}%");
        }
        $listProduct = $product->orderBy('price', 'DESC')->paginate(DEFAULT_PAGINATION_PER_PAGE);
        $brands = Brand::select('id', 'name', 'image', 'updated_at')
            ->orderBy('id', 'DESC')->get();

        return view('frontend.index', ['products' => $listProduct, 'brands' =>$brands, 'newProduct' => $newProduct, 'slide'=>$slide]);
    }

    public function detail($id)
    {
        $product = Product::select('id', 'quantity', 'image', 'name_vi AS name', 'desc_vi AS desc', 'price AS price');
        $product = $product->where('id', $id)->firstOrFail();
        return view('frontend.product_detail',['product' =>$product]);
    }

    public function about()
    {
        return view('frontend.about');
    }
    public function contact()
    {
        return view('frontend.contact');
    }
}
